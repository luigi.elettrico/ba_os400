// Bad Apple !! OS/400
// Ported by Luigi Elettrico

#include <time.h>
#include <recio.h>
#include <string.h>

#pragma mapinc("ba_display", "luigiel1/ba_display(bavramdisp)", "both", "")
#include "ba_display"

typedef LUIGIEL1_BA_DISPLAY_BAVRAMDISP_i_t input_t;
typedef LUIGIEL1_BA_DISPLAY_BAVRAMDISP_o_t output_t;

void BA_cleardisplay(output_t* o)
{
	memset(o->VD00, 0, 60);
	memset(o->VD01, 0, 60);
	memset(o->VD02, 0, 60);
	memset(o->VD03, 0, 60);
	memset(o->VD04, 0, 60);
	memset(o->VD05, 0, 60);
	memset(o->VD06, 0, 60);
	memset(o->VD07, 0, 60);
	memset(o->VD08, 0, 60);
	memset(o->VD09, 0, 60);
	memset(o->VD10, 0, 60);
	memset(o->VD11, 0, 60);
	memset(o->VD12, 0, 60);
	memset(o->VD13, 0, 60);
	memset(o->VD14, 0, 60);
	memset(o->VD15, 0, 60);
	memset(o->VD16, 0, 60);
	memset(o->VD17, 0, 60);
	memset(o->VD18, 0, 60);
	memset(o->VD19, 0, 60);
	memset(o->VD20, 0, 60);
	memset(o->VD21, 0, 60);
	memset(o->VD22, 0, 60);
}

void BA_readdecodeframe(FILE* fM, int nF, unsigned char* bpF)
{
	unsigned char a_bP[2] = {0x20, 0x3F};
	unsigned char a_bD[87];
	int i, p;

	memset(a_bD, 0, 87);
	
	fseek(fM, nF * 87, SEEK_SET);
	fread(a_bD, 1, 87, fM);
	
	for(i = 0; i < 87; ++i)
	{
		p = i << 4;
		bpF[p +  1] = bpF[p +  0] = a_bP[((a_bD[i] >> 7) & 1)];
		bpF[p +  3] = bpF[p +  2] = a_bP[((a_bD[i] >> 6) & 1)];
		bpF[p +  5] = bpF[p +  4] = a_bP[((a_bD[i] >> 5) & 1)];
		bpF[p +  7] = bpF[p +  6] = a_bP[((a_bD[i] >> 4) & 1)];
		bpF[p +  9] = bpF[p +  8] = a_bP[((a_bD[i] >> 3) & 1)];
		bpF[p + 11] = bpF[p + 10] = a_bP[((a_bD[i] >> 2) & 1)];
		bpF[p + 13] = bpF[p + 12] = a_bP[((a_bD[i] >> 1) & 1)];
		bpF[p + 15] = bpF[p + 14] = a_bP[((a_bD[i] >> 0) & 1)];
	}
}

void BA_displayframe(unsigned char* bpF, output_t* o)
{
	memcpy(o->VD00, &bpF[ 0 * 60], 60);
	memcpy(o->VD01, &bpF[ 1 * 60], 60);
	memcpy(o->VD02, &bpF[ 2 * 60], 60);
	memcpy(o->VD03, &bpF[ 3 * 60], 60);
	memcpy(o->VD04, &bpF[ 4 * 60], 60);
	memcpy(o->VD05, &bpF[ 5 * 60], 60);
	memcpy(o->VD06, &bpF[ 6 * 60], 60);
	memcpy(o->VD07, &bpF[ 7 * 60], 60);
	memcpy(o->VD08, &bpF[ 8 * 60], 60);
	memcpy(o->VD09, &bpF[ 9 * 60], 60);
	memcpy(o->VD10, &bpF[10 * 60], 60);
	memcpy(o->VD11, &bpF[11 * 60], 60);
	memcpy(o->VD12, &bpF[12 * 60], 60);
	memcpy(o->VD13, &bpF[13 * 60], 60);
	memcpy(o->VD14, &bpF[14 * 60], 60);
	memcpy(o->VD15, &bpF[15 * 60], 60);
	memcpy(o->VD16, &bpF[16 * 60], 60);
	memcpy(o->VD17, &bpF[17 * 60], 60);
	memcpy(o->VD18, &bpF[18 * 60], 60);
	memcpy(o->VD19, &bpF[19 * 60], 60);
	memcpy(o->VD20, &bpF[20 * 60], 60);
	memcpy(o->VD21, &bpF[21 * 60], 60);
	memcpy(o->VD22, &bpF[22 * 60], 60);
}

int main()
{
	input_t i;
	output_t o;
	
	_RFILE* dspf;
	FILE* fRawMv;
	
	unsigned char bpFrame[1392];
	
	int nFrame = 0;
	int nFrames = 6561;
	int isRunning = 1;
	
	fRawMv = fopen("luigiel1/ba_rawmv", "rb");
	dspf = _Ropen("luigiel1/ba_display", "rr+");
	
	BA_cleardisplay(&o);
	
	_Rformat(dspf, "BAVRAMDISP");
	_Rwrite(dspf, "", 0);
	
	memcpy(o.VD00, "                  Luigi Elettrico presents", 42);
	memcpy(o.VD01, "           'Bad Apple !!' but it`s OS/400 (IBM i)", 49);
	memcpy(o.VD02, "                   Depth 1 BPP, Res 30x23", 41);
	memcpy(o.VD04, "              ***=[ Press Enter to start ]=***", 46);
	
	_Rwrite(dspf, &o, sizeof(o));
	_Rreadn(dspf, &i, sizeof(i), __NO_LOCK);
	
	if(i.IN03 == 0xF1 || i.IN12 == 0xF1)
	{
	  _Rclose(dspf);
	  fclose(fRawMv);
	  
	  return 0;
	}
	
	while(nFrame < nFrames)
	{
		BA_readdecodeframe(fRawMv, nFrame, bpFrame);
		BA_displayframe(bpFrame, &o);
		
		_Rwrite(dspf, &o, sizeof(o));
		
		usleep(33366);
		++nFrame;
	}
	
	_Rclose(dspf);
	fclose(fRawMv);
	
	return 0;
}
